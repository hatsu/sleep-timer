const CHILD_PROCESS = window.require('child_process')

export const shutdown = (): void => {
    CHILD_PROCESS.exec('shutdown /s')
}

export const sleep = (): void => {
    CHILD_PROCESS.exec('rundll32.exe powrprof.dll, SetSuspendState Sleep')
}

export const lock = (): void => {
    CHILD_PROCESS.exec('rundll32.exe user32.dll,LockWorkStation')
}
