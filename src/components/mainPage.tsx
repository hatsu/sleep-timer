import { Grid, Typography, Box, Input, Button, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from '@mui/material'
import React, { ChangeEvent, useState, useRef, useEffect } from 'react'
import TimerIcon from '@mui/icons-material/Timer'
import video from '../media/Clock.mp4'
import { sleep, lock, shutdown } from '../helpers/cmd'

const Actions = ['sleep', 'lock', 'shutdown'] as const
type ActionType = typeof Actions

const MainPage = () => {
    const [timeLeft, setTimeLeft] = useState<number>(0)
    const [timeLeftSec, setTimeLeftSec] = useState<number>(0)
    const [interval, setInterval] = useState<number>(null)
    const [selectedAction, setSelectedAction] = useState<ActionType | string>('sleep')
    const videoRef = useRef(null)

    const onTimeLeftInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setTimeLeft(parseInt(event.target.value, 10))
        setTimeLeftSec(parseInt(event.target.value, 10) * 60)
    }
    const timerTick = () => {
        setTimeLeftSec((prevValue) => {
            setTimeLeft(Math.ceil((prevValue - 1) / 60))
            return prevValue - 1
        })
    }
    const onStartStopButtonClick = () => {
        if (interval) {
            window.clearInterval(interval)
            setInterval(null)
            videoRef.current.pause()
        } else {
            setInterval(window.setInterval(timerTick, 1000))
            videoRef.current.play()
        }
    }
    const onActionSelect = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSelectedAction(event.target.value)
    }

    useEffect(() => {
        if (interval) videoRef.current.play()
        else videoRef.current.pause()
    }, [interval])

    useEffect(() => {
        if (timeLeft <= 0 && interval) {
            window.clearInterval(interval)
            setInterval(null)
            videoRef.current.pause()
            switch (selectedAction) {
                case 'sleep':
                    sleep()
                    break
                case 'shutdown':
                    shutdown()
                    break
                case 'lock':
                    lock()
                    break
            }
        }
        document.title = `Sleep Timer (Time Left: ${timeLeftSec} seconds)`
    }, [timeLeftSec])

    return (
        <div>
            <video ref={videoRef} src={video} loop />
            <Box>
                <Typography variant="h3" align="center">
                    <TimerIcon fontSize="large" /> Sleep Timer <TimerIcon fontSize="large" />
                </Typography>
                <Box sx={{ mt: 2 }} display="flex" flexDirection="column" alignItems="center">
                    <Box sx={{ mt: 2 }}>
                        <Typography variant="subtitle2" component="span">
                            Time left:{' '}
                            <Input
                                value={interval ? timeLeftSec : timeLeft}
                                size="small"
                                onChange={onTimeLeftInputChange}
                                inputProps={{
                                    step: 1,
                                    min: 0,
                                    max: 1440,
                                    type: 'number',
                                }}
                                sx={{ width: '60px', color: 'white' }}
                                disabled={!!interval}
                            />{' '}
                            {interval ? ' Seconds' : ' Minutes'}
                        </Typography>
                    </Box>
                    <Box sx={{ mt: 2 }}>
                        <FormControl component="fieldset">
                            <RadioGroup row aria-label="action" name="row-radio-buttons-group" value={selectedAction} onChange={onActionSelect}>
                                <FormControlLabel value="sleep" control={<Radio color="warning" />} label="Sleep" />
                                <FormControlLabel value="lock" control={<Radio color="warning" />} label="Lock" />
                                <FormControlLabel value="shutdown" control={<Radio color="warning" />} label="Shutdown" />
                            </RadioGroup>
                        </FormControl>
                    </Box>
                    <Box sx={{ mt: 2 }}>
                        <Button id="start-stop-button" size="large" variant="outlined" color="warning" onClick={onStartStopButtonClick} disabled={timeLeft <= 0}>
                            Start | Stop
                        </Button>
                    </Box>
                </Box>
            </Box>
        </div>
    )
}
export default MainPage
