import React from 'react'
import ReactDOM from 'react-dom'
import MainPage from './components/mainPage'
import { ThemeProvider, createTheme } from '@mui/material'
const appContainer = document.querySelector('#app')
const theme = createTheme({
    palette: {
        mode: 'dark',
    },
})

function render() {
    ReactDOM.render(
        <ThemeProvider theme={theme}>
            <MainPage />
        </ThemeProvider>,
        appContainer
    )
}

render()
