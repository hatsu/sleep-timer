# Sleep Timer

**Sleep Timer is a Windows App to lock, shutdown or to send to sleep your PC by a timer. Set up a timer, press start button and app will fire selected action on timer's end.**

**Installer for Windows: [SleepTimerInstaller(x64)](https://drive.google.com/file/d/1tvhyKa-VQzluywnhKifFbaWZU9EH8KA4/view?usp=sharing)**

Built with: Electron, TypeScript, React, MUI
